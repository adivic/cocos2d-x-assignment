## Cocos2d-X Assignmnet
![Assignment](/uploads/430ecfb89dd3af2c1c58ca5185313a37/Assignment.PNG)
### Structure
In this Assignmnet, I used 2 classes to implement given task. First class is GameScene which is main class to show game scene.
Inside it has private functions for adding sprites and menu items to the viewport. Those are used to achieve cleaner code and easy to use later. Furthermore, it has a private character variable, that represents 'character' on the scene. 
The Character class contains logic for spawning character sprite on the scene and functions to implement jump callback
and update it's position.

In the GameScene there's few add sprites to populate background. They are: 
- Tree trunk
- Pine tree 
- Fisherman
- Sky that uses Parallax scrolling

When player clicks on anywhere inside window, sky will move accordingly.

![assignment_gif](/uploads/e62a1614cc409749576f541c8a7a81e5/assignment_gif.mp4)

Zip project link: https://drive.google.com/file/d/13mgbAwTQlZSqSwNrKZvVaHzm0EMyIa10/view?usp=drive_link
