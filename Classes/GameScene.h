#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"

class Character;

class GameScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    virtual void update(float DeltaTime) override;

	/**
	 * Exit button callback
	 */
    void MenuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameScene);

    virtual ~GameScene();

protected:
    
	/**
	* Helper function for adding Sprite on screen
	*/
    cocos2d::Sprite* AddSprite(const std::string& Filename, cocos2d::Vec2 Position, int SpriteOrder, float Scale = 1.f);

    /**
    * Helper function for adding ImageItem on screen
    */
    void AddImageItem(const std::string& NormalFilename, const std::string& SelectedFilename, const cocos2d::ccMenuCallback, cocos2d::Vec2& Position, bool bUseSpriteSizeOffset = false);

private:
    Character* character;
};

#endif // __GAME_SCENE_H__
