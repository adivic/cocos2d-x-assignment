#include "Background.h"
#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;

bool BackgroundScene::init()
{

	if (!Scene::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	// Create the first ParallaxNode
	parallaxNode1 = ParallaxNode::create();
	this->addChild(parallaxNode1);

	// Create and add layers to the first ParallaxNode
	auto layer1a = Layer::create();
	auto sprite1a = Sprite::create("sky.png");
	sprite1a->setPosition(Vec2(0, visibleSize.height / 2));
	layer1a->addChild(sprite1a);

	auto layer1b = Layer::create();
	auto sprite1b = Sprite::create("sky.png");
	sprite1b->setPosition(Vec2(sprite1a->getContentSize().width, visibleSize.height / 2));
	layer1b->addChild(sprite1b);

	parallaxNode1->addChild(layer1a, 1, Vec2(1.0f, 0), Vec2::ZERO);
	parallaxNode1->addChild(layer1b, 1, Vec2(1.0f, 0), Vec2::ZERO);
	
	// Enable mouse events
	auto listener = EventListenerMouse::create();
	listener->onMouseDown = CC_CALLBACK_1(BackgroundScene::onMouseDown, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	return true;
}

void BackgroundScene::onMouseDown(EventMouse* event)
{
	// Get the mouse click coordinates
	Vec2 mousePosition = Vec2(event->getCursorX(), event->getCursorY());

	// Convert mouse coordinates to the coordinates relative to the scene
	mousePosition = this->convertToNodeSpace(mousePosition);

	// Calculate the new position for both parallax nodes based on the click position
	float newXPosition = mousePosition.x;

	float minX = 0.0f;
	float maxX = Director::getInstance()->getVisibleSize().width/2;

	// If the click is on the right side of the screen, move to the right halfway
	if (mousePosition.x > Director::getInstance()->getVisibleSize().width / 2)
	{
		newXPosition = maxX + (parallaxNode1->getPositionX() - maxX) * 0.5f;
	}
	else
	{
		// If the click is on the left side of the screen, move to the left halfway
		newXPosition = minX + (parallaxNode1->getPositionX() - minX) * 0.5f;
	}

	float transitionDuration = 1.0f;

	// Create MoveTo actions for both parallax nodes
	auto moveAction1 = MoveTo::create(transitionDuration, Vec2(newXPosition, parallaxNode1->getPositionY()));

	// Run the actions on the parallax nodes
	parallaxNode1->runAction(moveAction1);

}
