#pragma once

#ifndef __BACKGROUND_SCENE_H__
#define __BACKGROUND_SCENE_H__

#include "cocos2d.h"

class BackgroundScene : public cocos2d::Scene
{
private:
	cocos2d::ParallaxNode* parallaxNode1;

public:
	virtual bool init() override;

	void onMouseDown(cocos2d::EventMouse* event);
	
	CREATE_FUNC(BackgroundScene);
};

#endif // __BACKGROUND_SCENE_H__
