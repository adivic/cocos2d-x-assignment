#include "GameScene.h"
#include "Character.h"
#include "Background.h"

USING_NS_CC;

Scene* GameScene::createScene()
{
    return GameScene::create();
}

bool GameScene::init()
{
    if (!Scene::init())
    {
        return false;
    }

    scheduleUpdate();

    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    // Add "Quit" icon to exit the progress
	Vec2 exitPosition(origin.x + visibleSize.width, visibleSize.height);
    AddImageItem("CloseNormal.png", "CloseSelected.png", CC_CALLBACK_1(GameScene::MenuCloseCallback, this), exitPosition, true);
	

    // Add scrollable sky
	auto bg = BackgroundScene::create();
	addChild(bg);
    
    // Add background sprite
    AddSprite("background.png", Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y), 0);
	// Add Tree on other side of the lake
    AddSprite("pinetree.png", Vec2(origin.x + visibleSize.width / 2 + 50, visibleSize.height / 2 + origin.y + 120), 0, 0.65f);
    // Add tree trunk
	AddSprite("TreeStump.png", Vec2(origin.x + visibleSize.width / 2 + 40, visibleSize.height / 2 + origin.y - 120), 0, 0.4f);
    //// Add fisherman 
    AddSprite("fisherman.png", Vec2(origin.x + visibleSize.width / 2 + 180, visibleSize.height / 2 + origin.y), 0, 0.25f);

    // Initialize character sprite
    character = new Character (this);

    // Add Jump button and add callback for the jump
	Vec2 jumpBtnPosition(origin.x + visibleSize.width * 0.25f, 25);
	AddImageItem("jump.png", "jump.png", CC_CALLBACK_1(Character::Jump, character), jumpBtnPosition);

    return true;
}


void GameScene::update(float DeltaTime)
{
    if (character)
    {
        character->Update(DeltaTime);
    }
}

void GameScene::MenuCloseCallback(Ref* pSender)
{
    // Quit Game
    Director::getInstance()->end();
}

GameScene::~GameScene()
{
    if (character)
    {
        delete character;
    }
}

cocos2d::Sprite* GameScene::AddSprite(const std::string& Filename, cocos2d::Vec2 Position, int SpriteOrder, float Scale /*= 1.f*/)
{
    cocos2d::Sprite* sprite = Sprite::create(Filename);
    if (sprite == nullptr)
    {
        return nullptr; 
    }

    sprite->setPosition(Position);
    sprite->setScale(Scale);
	addChild(sprite, SpriteOrder);
    return sprite;
}

void GameScene::AddImageItem(const std::string& NormalFilename, const std::string& SelectedFilename, const cocos2d::ccMenuCallback Callback, cocos2d::Vec2& Position, bool bUseSpriteSizeOffset/* = false*/)
{
	cocos2d::MenuItemImage* item = MenuItemImage::create(NormalFilename, SelectedFilename, Callback);

	if (item == nullptr || item->getContentSize().width <= 0 || item->getContentSize().height <= 0)
	{
        return;
	}

	if (bUseSpriteSizeOffset)
	{
        Position.x -= item->getContentSize().width / 2;
        Position.y -= item->getContentSize().height / 2;
	}

    item->setPosition(Position);

	// create menu, it's an autorelease object
	cocos2d::Menu* menu = Menu::create(item, NULL);
	menu->setPosition(Vec2::ZERO);
	addChild(menu, 1);
}
