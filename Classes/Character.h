#ifndef __CHARACTER_SPRITE_H__
#define __CHARACTER_SPRITE_H__

#include "cocos2d.h"

class Character
{
private:

	cocos2d::Vec2 Origin;
	cocos2d::Size VisibleSize;
	
	/**
	* Player sprite reference
	*/
	cocos2d::Sprite* CharacterSprite;
	
	/**
	* Initial player position, where player sprite was created
	*/
	cocos2d::Vec2 CharacterInitaliPosition;
	
	/**
	* Jump height. player will jump till this height. His current location + JumpHeight.
	*/
	float JumpHeight;
	
	/**
	* Jump update speed.
	*/
	
	float JumpVelocity;
	
	/**
	* Speed that will bring player sprite down, after reaching desired height from jump.
	*/
	float GravityForce;
	
	/**
	* Bool variable to mark that player has reached desired 'jump' height
	*/
	bool bReachHeight;
	
	/**
	* Bool variable marks that player should perform jump
	*/
	bool bShouldJump;

public:
	Character(cocos2d::Scene* scene);

	/**
	* Update - Called within GameScene update to sprite position
	*/
	void Update(float DeltaTime);
	
	/**
	* Called as a callback function from the GameScene, when button is created.
	* Sets bShouldJump to true
	*/
	void Jump(cocos2d::Ref* pSender);

private:
	/**
	* Updates sprite location upwards. Called from Update
	*/
	void JumpMovement(float DeltaTime);

	/**
	* Updates sprite location downwards, e.g. applying gravity after jump. 
	* Called from Update
	*/
	void Fall(float DeltaTime);
};

#endif // __CHARACTER_SPRITE_H__
