#include "Character.h"

USING_NS_CC;

Character::Character(cocos2d::Scene* scene)
{
	Origin = Director::getInstance()->getVisibleOrigin();
	VisibleSize = Director::getInstance()->getVisibleSize();
	// Set Default values for variables
	JumpVelocity = 512.f;
	GravityForce = 650.f;
	JumpHeight = 150.f;
	// Set Character initial size in bottom left corner
	CharacterInitaliPosition = Vec2(Origin.x + VisibleSize.width * 0.25f, Origin.y + VisibleSize.height * 0.4f);
	// Create Character sprite
	CharacterSprite = Sprite::create("character.png");
	CharacterSprite->setPosition(CharacterInitaliPosition);

	scene->addChild(CharacterSprite, 1);
}

void Character::Update(float DeltaTime)
{
	if (bShouldJump && !bReachHeight)
	{
		JumpMovement(DeltaTime);
	}

	if (bReachHeight)
	{
		Fall(DeltaTime);
	}
}

void Character::Jump(cocos2d::Ref* pSender)
{
	bShouldJump = true;
}

void Character::JumpMovement(float DeltaTime)
{
	if (CharacterSprite == nullptr)
	{
		return;
	}

	Vec2 position = CharacterSprite->getPosition();
	position.y += JumpVelocity * DeltaTime;
	if (position.y > CharacterInitaliPosition.y + JumpHeight)
	{
		bReachHeight = true;
		bShouldJump = false;
	}

	CharacterSprite->setPosition(position);
}

void Character::Fall(float DeltaTime)
{
	if (CharacterSprite == nullptr)
	{
		return;
	}

	Vec2 position = CharacterSprite->getPosition();
	position.y -= GravityForce * DeltaTime;
	if (position.y <= CharacterInitaliPosition.y)
	{
		bReachHeight = false;
		bShouldJump = false;
	}

	CharacterSprite->setPosition(position);
}